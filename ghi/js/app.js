window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            console.log("Something broke");
        } else {
            const data = await response.json()
            const conference = data.conferences[0];
            const nameTag = document.querySelector('.card-title');
            nameTag.innerHTML = conference.name;

            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const detail = await detailResponse.json();
                const description = detail.conference.description
                const descriptionTag = document.querySelector('.card-text')
                descriptionTag.innerHTML = description

                const imageTag = document.querySelector('.card-img-top')
                imageTag.src = detail.conference.location.picture_url;

                console.log(detail);


            }
        }
    } catch(error) {
        console.error("Houston we have a problem", error)
    }
});


/* <div class="card">
            <img src="..." class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">Conference name</h5>
              <p class="card-text">Conference description</p>
            </div>
          </div> */ 
